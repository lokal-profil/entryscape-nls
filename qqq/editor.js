define({
    "externalAndLocalMDEditorTitle" : "Dialog title on metadata editor for both original and locally provided metadata.",
    "LocalMDEditorTitle" : "Dialog title on metadata editor when there is only locally provided metadata.",
    "mandatoryLabel" : "Label on button for showing only mandatory metadata in metadata editor dialog.",
    "recommendedLabel" : "Label on button for showing mandatory and recommended metadata in metadata editor dialog.",
    "optionalLabel" : "Label on button for all metadata in metadata editor dialog.",
    "colorLabel" : "Label for coloring metadata fields according to mandatory, recommended and optional in metadata editor dialog.",
    "dialogCancelLabel" : "Label on cancel button (abandon changes) in metadata editor dialog.",
    "dialogDoneLabel" : "Label on button for saving the changes in metadata editor dialog.",
    "dialogDoneBusyLabel" : "In-progress label on button, is shown while we are waiting for metadata save request to be processed in metadata editor dialog.",
    "failedSavingUnsufficientMDRights" : "Pop-up message upon failure to save metadata due to a lack of access rights, may be shown in metadata editor dialog after save/done button is pressed.",
    "modifiedPreviouslyOnServer" : "Pop-up message upon failure to save metadata in metadata dialog due to concurrent modification, e.g. someone happened to save the same metadata while the user was editing it.",
    "metadataSaved" : "Pop-up message when user successfully saved metadata via metadata editor dialog."
});
