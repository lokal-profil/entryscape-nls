define({
    "memberOfHeader" : "Tab name showing communities / groups that the user is member of.",
    "membershipHeader" : "Tab name showing members of a community / group.",
    "folderWithAccessHeader" : "Tab name showing folders where the current user or group / community have specific access right set.",
    "latestMaterial" : "Tab name showing material that was recently updated in the current user / groups home portfolio.",
    "featuredMaterial" : "Tab name showing material that was explicitly featured in the current user / groups home portfolio.",
    "noFeaturedMaterial" : "Message that there is no featured material to show."
});
