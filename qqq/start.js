define({
    "peopleTab": "Name of tab to show people with accounts in EntryScape",
    "communitiesTab": "Name of tab to show groups / communities in EntryScape",
    "recentMaterialTab": "Name of tab that shows recently created or modified material in EntryScape.",
    "searchCommunities": "Placeholder text in searchfield for searching for communities/groups",
    "searchPeople": "Placeholder text in searchfield for searching for people."
});
