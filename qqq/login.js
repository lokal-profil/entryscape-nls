define({
    "title" : "Title of login dialog",
    "user" : "Label before inputfield for username in login dialog." ,
    "password" : "Label before inputfield for password in login dialog.",
    "logIn" : "Text on login button in login dialog.",
    "statusLoggingIn" : "Message to show on login button in login dialog while logging in, should end with &hellip; (ellipsis)",
    "statusLoggingOut" : "Message to show on login button in login dialog while signing out, should end with &hellip; (ellipsis)",
    "statusLoggedOut" : "Message showing that you are currently signed out (and ready to log in again) in the login dialog.",
    "statusInvalidLogin" : "Message that the attempt to log in via the login dialog failed due to wrong credentials.",
    "warning" : "Label before a warning message in the login dialog.",
    "warningText" : "Browser incompatability warning in login dialog.",
    "andHigher" : "Text to append (in the warning section in the login dialog) to a browser version number to indicate that it works with later versions.",
    "staySignedInLabel": "Checkbox text to indicate that the user should stay signed in in the login dialog.",
    "openIdLoginLabel": "Text above the alternative sign in mechanisms in login dialog.",
    "signup": "Text that proviedes the option to sign up if the user does not already have an account, in login dialog.",
    "signupInfo": "Longer text that tells the user that he can sign up using an existing identity in another identity provider, in signup view."
});
