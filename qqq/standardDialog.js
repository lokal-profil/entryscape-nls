define({
    "cancelButtonLabel" : "Generic cancel button text in standard dialog reused across EntryScape.",
    "finishButtonLabel" : "Generic finish button text in standard dialog reused across EntryScape.",
    "doneButtonLabel": "Generic done button text in standard dialog reused across EntryScape.",
    "busyButtonLabel" : "Generic processing request text in standard dialog reused across EntryScape. E.g. while sending or saving information to the repository."
});
