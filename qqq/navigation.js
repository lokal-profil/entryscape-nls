define({
    "aboutLink" : "Link-text to about information in navigationbar dropdown-menu.",
    "helpLink" : "Link-text to help page in navigationbar dropdown-menu.",
    "greeting" : "Greeting text in navigationbar dropdown-menu, the text should contain the variabel 'user' corresponding to the current signed in user, or guestUser if none is signed in.",
    "guestUser" : "Name for 'guest user', i.e. none is signed in, in greetings text in navigationbar dropdown-menu.",
    "homeLink" : "Link-text for the link going to your work view, e.g. the folder view. The link is shown in the navigationbar dropdown-menu.",
    "profileLink": "Link-text for the link going to your personal overview page / profile page. The link is shown in the navigationbar dropdown-menu.",
    "settings" : "Link-text for the link going to the setting view for the currently signed in user. The link is shown in the navigationbar dropdown-menu.",
    "searchLabel" : "Tooltip on search icon in navigationbar.",
    "searchFieldMessage" : "Placeholder text in search inputfield in navigationbar.",
    "languageLabel" : "Label before language selection dropdown in navigationbar dropdown-menu.",
    "logIn": "Button/link text for signing in, both directly in navigationbar and in navigationbar dropdown-menu.",
    "logOut": "Sign out button text in navigationbar dropdown-menu."
});
