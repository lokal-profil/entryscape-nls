define({
	"root" : {
        "title" : "Sign in to ${app}",
        "user" : "Username:" ,
        "password" : "Password:",
        "logIn" : "Sign In",
        "statusLoggingIn" : "Signing in&hellip;",
        "statusLoggingOut" : "Signing out&hellip;",
        "statusLoggedOut" : "You are now signed out.",
        "statusInvalidLogin" : "Invalid user name or password.",
        "warning" : "Warning: ",
        "warningText" : "Your browser has not been tested with this version of EntryScape. While EntryScape will probably work, there may be some issues. The following browsers have been tested:",
        "andHigher" : "%s and higher",
        "staySignedInLabel": "Stay signed in",
        "openIdLoginLabel": "Or, sign in with an existing identity:",
        "signup": "If you do not have an account, sign up now!",
        "signupInfo": "You can create an account by reusing an existing identity from one of the following services:"
	},
    "sv": true
});
