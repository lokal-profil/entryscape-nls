define({
    "root" : {
        "resourceLabel": "Resource:",
        "informationLabel": "Information:",
        "extInformationText" : "Original information",
	"locInformationText" : "Provided information",
	"creatorText" : "Creator",
	"accessText" : "Access",
	"publicAccess": "Public",
	"sharedAccess": "Shared",
	"privateAccess": "Private",
	"modifiedText" : "Modified",
	"createdText" : "Created",
	"referentsLabel" : "In folders:",
        "edit": "Edit",
        "present": "Present",
        "embed": "Embed",
        "webpage": "Webpages",
        "pdf": "Pdf:s",
        "flash": "Flash files",
        "image": "Images",
        "unknown": "not specified",
        "loadingImage": "Loading image...",
        "failedLoadingImage": "Failed loading image with address:<br> {url}"
    },
    "sv": true
});
