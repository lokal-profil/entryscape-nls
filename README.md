# EntryScape Native Language Support (NLS)

To simplify the translation of EntryScape into multiple languages we have separated out the
localization strings into a separate project.

## Contributing translations
EntryScape localization strings are maintained on [https://translatewiki.net/](https://translatewiki.net/).
Hence, to contribute translations you have to register and translate strings there.
