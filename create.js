define({
	"root" : {
        "createDialogHeader": "Create and add an entry to the current folder.",
        "fileLabel": "File",
        "browseLabel": "Browse ...",
        "addressLabel": "Address",
        "nameLabel": "Name",
        "createTypeLabel" : "Type",
		"selectTypeWaiting" : "No type autodetected or manually selected yet.",
        "artifactTypeRequest": "You must chose a type below.",
        "clearManuallySelectedType": "Clear manually selected type.",
        "clearAutoSelectedType": "clear autodetected type.",
        "noAutodetectTypePossible": "Autodetect failed, select manually instead.",
        "addressIsInvalid": "The provided value is is not a valid webaddress.",
        "missingAddress": "Please provide a webaddress",
        "missingLabel": "Please provide a name",
        "createArtifact": "Create an artifact",
        "createLink": "Create a link",
        "uploadFile": "Upload a file",
        "createText": "Create a text document",
        "createFolder": "Create a Folder",
        "unableToCreateFolderErrorMessage" : "Unable to create folder. Please make sure you have\n sufficient access rights to the current folder.",
        "unableToCreateTextErrorMessage" : "Unable to create text document. Please make sure you have\n sufficient access rights to the current folder."
	},
    "sv": true
});
