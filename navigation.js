define({
	"root" : {
        "aboutLink" : "About",
        "helpLink" : "Help",
        "greeting" : "Welcome {user}",
        "guestUser" : "Guest",
        "homeLink" : "My folders",
        "profileLink": "My home",
        "settings" : "Settings",
        "searchLabel" : "Search",
        "searchFieldMessage" : "Type search terms here",
        "languageLabel" : "Language",
        "logIn": "Sign In",
        "logOut": "Sign Out"
    },
    "sv": true
});
