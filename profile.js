define({
    "root" : {
	"memberOfHeader" : "Communities",
	"membershipHeader" : "Members",
        "folderWithAccessHeader" : "Folders",
	"latestMaterial" : "Recent material",
        "featuredMaterial" : "Featured material",
        "noFeaturedMaterial" : "No featured material to show."
	},
    "sv": true
});
