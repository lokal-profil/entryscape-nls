define({
    "title": "${app} Inloggning",
    "user": "Användare:",
    "password": "Lösenord:",
    "logIn": "Logga in",
    "statusLoggingIn": "Loggar in&hellip;",
    "statusLoggingOut": "Loggar ut&hellip;",
    "statusLoggedOut": "Du är nu utloggad.",
    "statusInvalidLogin": "Ogiltigt användarnamn eller lösenord.",
    "warning": "Varning: ",
    "warningText": "Din webbläsare har inte testats med denna version av Confolio. Även om Confolio troligtvis kommer att fungera kan det finnas brister. Testade webbläsare:",
    "andHigher": "%s och högre",
    "staySignedInLabel": "fortsätt vara inloggad",
    "openIdLoginLabel": "Eller logga in med en existerande användare:",
    "signup": "Om du inte redan har en användare, registrera dig nu!",
    "signupInfo": "Du kan skapa ett konto genom att utnyttja inloggningen från en av dessa tjänster:"
});
