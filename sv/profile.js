define({
    "memberOfHeader" : "Grupper",
    "membershipHeader" : "Medlemmar",
    "folderWithAccessHeader" : "Foldrar",
    "latestMaterial" : "Senaste materialet",
    "featuredMaterial" : "Utvalt material",
    "noFeaturedMaterial" : "Inget utvalt material att visa."
});
