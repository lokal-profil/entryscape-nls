define({
    "aboutLink": "Om",
    "helpLink": "Hjälp",
    "greeting": "Välkommen {user}",
    "guestUser": "Gäst",
    "homeLink": "Mina kataloger",
    "profileLink": "Mitt hem",
    "settings": "Inställningar",
    "searchLabel": "Sök",
    "searchFieldMessage": "Skriv sökord här",
    "languageLabel": "Språk",
    "logIn": "Logga in",
    "logOut": "Logga ut"
});
