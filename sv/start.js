define({
    "peopleTab": "Personer",
    "communitiesTab": "Gemenskaper",
    "recentMaterialTab": "Senaste material",
    "searchCommunities": "Sök efter gemenskaper",
    "searchPeople": "Sök efter personer"
});
