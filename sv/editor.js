define({
    "externalAndLocalMDEditorTitle" : "Ursprunglig information (vänstra sidan, ej redigerbar). &nbsp;&nbsp;&nbsp;Tillhandahållen information (högra sidan, redigerbar).",
    "LocalMDEditorTitle" : "Redigera information",
    "mandatoryLabel": "Obligatorisk",
    "recommendedLabel": "Rekommenderad",
    "colorLabel": "Färga fältnamn",
    "optionalLabel": "Valfri",
    "dialogCancelLabel": "Avbryt",
    "dialogDoneLabel": "Klar",
    "dialogDoneBusyLabel": "Sparar",
    "failedSavingUnsufficientMDRights": "Du har inte tillräckliga rättigheter för att spara metadatan.",
    "modifiedPreviouslyOnServer": "Fel: Kunde inte spara dina ändringar, eftersom någon annan har ändrat innan du hann spara",
    "metadataSaved" : "Informationen har sparats för resursen"
});
