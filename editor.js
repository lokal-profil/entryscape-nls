define({
	"root" : {
		"externalAndLocalMDEditorTitle" : "Original information (left side, not editable). &nbsp;&nbsp;&nbsp;Provided information (right side, editable).",
		"LocalMDEditorTitle" : "Edit information",
        "mandatoryLabel" : "Mandatory",
        "recommendedLabel" : "Recommended",
        "colorLabel" : "Color labels",
        "optionalLabel" : "Optional",
        "dialogCancelLabel" : "Cancel",
        "dialogDoneLabel" : "Done",
        "dialogDoneBusyLabel" : "Saving",
        "failedSavingUnsufficientMDRights" : "You do not have sufficient rights to save the metadata.",
        "modifiedPreviouslyOnServer" : "ERROR: The resource has been changed on the server, your changes have NOT been saved.",
        "metadataSaved" : "Information has been saved for resource "
    },
    "sv": true
});